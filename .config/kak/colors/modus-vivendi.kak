# code
set-face global comment white+i
set-face global string bright-blue
set-face global value bright-cyan
set-face global keyword bright-magenta
set-face global variable cyan
set-face global function magenta
set-face global builtin bright-white+b
set-face global operator red
set-face global attribute white+b
set-face global type bright-cyan
set-face global module bright-blue+i
set-face global meta red+i

# UI
set-face global Information MenuBackground
set-face global LineNumberCursor bright-white
set-face global LineNumbers bright-black
set-face global MenuBackground white,rgb:222222,default
set-face global MenuForeground black,white,default
set-face global MenuInfo black,bright-blue
set-face global PrimaryCursorEol default,green
set-face global PrimarySelection default+r
